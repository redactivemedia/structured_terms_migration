<?php

namespace Drupal\structured_terms_migration\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\taxonomy\Entity\Term;

/**
 * @MigrateProcessPlugin(
 *   id = "structured_terms_migration",
 *   handle_multiples = FALSE
 * )
 *
 * @codingStandardsIgnoreStart
 *
 * To create nested terms from string 'top/middle/lowest':
 * @code
 * field_dragdrop_section:
 *   plugin: structured_terms_migration
 *   source: string
 *   vocabulary: 'vocabulary_machine_name'
 * @endcode
 *
 * @codingStandardsIgnoreEnd
 */
class StructuredTermsMigration extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($path, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    echo $path . "\n\n";

    // Make vocabulary required.
    if (!isset($this->configuration['vocabulary'])) {
      throw new MigrateException('"vocabulary" must be configured.');
    }

    // Skip if no structure data set.
    if (empty($path)) {
      return FALSE;
    }

    // Default to having no parent.
    $parent_term_tid = 0;

    $vocabulary = $this->configuration['vocabulary'];

    // $value will come in with the last part being the article slug.
    //
    // To prevent creation of several thousand site_structure taxonomy terms,
    // we need to trim leading and trailing slashes, and the last part of the
    // path.
    //
    // An example of $value is: /features/student/actuary-of-the-future/actuary-of-the-future-dalisu-dube-of-etana-insurance/.
    $path = trim($path, '/');

    // Turn the path into an array.
    $path_array = explode('/', trim($path, '/'));

    // Remove the last part of the path.
    array_pop($path_array);

    if (!empty($path_array)) {
      foreach ($path_array as $slug) {
        $term_properties = [
          'name' => $slug,
          'vid' => $vocabulary,
          'parent' => $parent_term_tid,
        ];

        // Lookup term.
        $slug_terms = \Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->loadByProperties($term_properties);

        // If term for this part of slug not found, create it.
        if (empty($slug_terms)) {
          // Create term.
          $term_properties['field_sitestructure_urlslug'] = substr($slug, 0, 30);

          $slug_term = Term::create($term_properties);
        }
        else {
          // Use existing term.
          $slug_term = reset($slug_terms);

          // Update existing term with the slug. Max length 30 characters.
          $slug_term->field_sitestructure_urlslug->setValue(substr($slug, 0, 30));
        }

        $slug_term->save();
        $parent_term_tid = $slug_term->id();
      }
    }

    // Return tid of deepest nested term.
    return $parent_term_tid;
  }

}
